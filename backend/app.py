import logging
import aiohttp
from aiohttp import web


class Hub:
    def __init__(self):
        self._web_sockets = []

    def join(self, socket):
        logging.info(f'Client joined')
        self._web_sockets.append(socket)

    async def broadcast(self, payload: dict):
        for socket_user in self._web_sockets:
            await self._send_data(socket_user, payload)

    async def _send_data(self, ws, data: dict):
        if ws.closed:
            self._web_sockets.remove(ws)
            return
        try:
            await ws.send_str(data)
        except ConnectionResetError:
            self._web_sockets.remove(ws)

hub = Hub()


async def websocket_handler(request):
    logging.debug('handler is handling')
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    hub.join(ws)
    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            await hub.broadcast(msg.data)
        elif msg.type == aiohttp.WSMsgType.ERROR:
            logging.info('ws connection closed with exception %s' % ws.exception())
    logging.debug('websocket connection closed')
    return ws


app = web.Application()

app.add_routes([
    web.get('/ws', websocket_handler),
])

logging.basicConfig(level=logging.DEBUG)

web.run_app(
    app,
    port=8080,
)

