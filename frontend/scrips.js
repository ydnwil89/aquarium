import {getCtx} from './canvas.js';
//import socket from './socket.js'

const ctx = getCtx();

var fishesCounter = document.getElementById('fishesCounter');
var additionalInfo = document.getElementById('additionalInfo');
const fishes = [];
const clown = {
    id: 1,
    skinRight: document.getElementById('fish6'),
    skinLeft: document.getElementById('fish5')
}
const plotva = {
    id: 2,
    skinRight: document.getElementById('fish'),
    skinLeft: document.getElementById('fish2')
}
const tropical = {
    id: 3,
    skinRight: document.getElementById('fish7'),
    skinLeft: document.getElementById('fish8')
}
const gold = {
    id: 4,
    skinRight: document.getElementById('fish4'),
    skinLeft: document.getElementById('fish3')
}
const fishTypes = [clown, plotva, tropical, gold]

class Fish {
    constructor(x, y, size, type) {
        this.x = x
        this.y = y
        this.startY = y;
        this.size = size
        this.vx = 1
        this.direction = this.vx
        this.type = type
    }

    move() {
        this.x = this.x + this.vx
        this.y = Math.sin(this.x / 20) * 20 + this.startY;
        
        if (ctx.canvas.width <= this.x + this.size) {
            this.vx *= -1
        }
        if (this.x <= 0) {
            this.vx *= -1
        }
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

testButton.onclick = function addFish() {
    const size = Math.random() * 200 + 50
    var fishType = getRandomInt(4);
    var fish = new Fish(
        Math.random() * (ctx.canvas.width - size),
        Math.random() * (ctx.canvas.height - size),
        size,
        fishTypes[fishType],
    );
    //socket.send(JSON.stringify(fish));
    fishes.push(fish);
    fishesCounter.innerHTML = 'Fishes: ' + fishes.length;
}

function drawAdditionalInfo (fishes) {
    var clownsCounter = 0;
    var plotvasCounter = 0;
    var tropicalsCounter = 0;
    var goldenCounter = 0;
    for(let fish of fishes) {
        if (fish.type == clown) {
            clownsCounter++;
        }
        else if (fish.type == plotva) {
            plotvasCounter++;
        }
        else if (fish.type == tropical) {
            tropicalsCounter++;
        }
        else if (fish.type == gold) {
            goldenCounter++;
        }
    }
    additionalInfo.innerHTML = 'Additional info' + '\n' + `Clowns: ${clownsCounter}` + '\n' + `Plotvas: ${plotvasCounter}` + '\n' + `Tropicals: ${tropicalsCounter}` + '\n' + `Golden: ${goldenCounter}`;
}

//socket.onmessage = function(event) {
//      const fish = JSON.parse(event.data);
//      fishes.push(Object.assign(new Fish(), fish));
//};

function drawFishes(fishes) {
    for(let fish of fishes){
        if (fish.vx < 0) {
            var img = fish.type.skinLeft;//document.getElementById("fish3");
            ctx.drawImage(img, fish.x, fish.y, fish.size, fish.size)
        }
        else {
            var img = fish.type.skinRight;//document.getElementById("fish4");
            ctx.drawImage(img, fish.x, fish.y, fish.size, fish.size)
        }
        //ctx.beginPath();
        //ctx.rect(fish.x, fish.y, fish.size, fish.size);
        //ctx.fillStyle = "red";
        //ctx.fill();
        //ctx.closePath();
    }
}


function clear() {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function draw() {
    clear();
    for(let fish of fishes){
        fish.move();
    }
    drawFishes(fishes);
    drawAdditionalInfo(fishes);
}
setInterval(draw, 10);
